<?php

/**
 * Sanitise some html by stripping all tags except those in the whitelist
 *
 * @param $content
 *
 * @return string
 */
function sanitizeHtml($content) {
    $whitelist = [
        // High level - TODO: research if this is actually needed
        'html', 'body',

        // Separators
        'br', 'hr', 'p',

        // Modifiers
        'b', 'em', 'i', 'u', 's', 'span', 'a',

        // Lists
        'ul', 'ol', 'li',

        // Tables
        'table', 'tr', 'td'
    ];

    return strip_tags($content, implode(',', $whitelist));
}
