<?php

/**
 * Get all the image sizes in an array that mimics get field
 * for usage with the featured image of a post...
 *
 * @param int $post_id
 *
 * @return array|bool
 */
function get_featured_image_object($post_id = 0){
    $image = array();

    if(!$post_id){
        return false;
    }

    $attachment_id = get_post_thumbnail_id($post_id);

    $args = [
        'post_type' => 'attachment',
        'include' => $attachment_id,
    ];

    $images = get_posts($args);

    if($images){
        $alt = get_post_meta($attachment_id, '_wp_attachment_image_alt', true);
        $url = wp_get_attachment_image_src($attachment_id, 'full', false);

        $image['title'] = $images[0]->post_title;
        $image['description'] = $images[0]->post_content;
        $image['caption'] = $images[0]->post_excerpt;
        $image['alt'] = $alt;
        $image['url'] = $url[0];

        foreach(get_intermediate_image_sizes() as $size){
            $s = wp_get_attachment_image_src($attachment_id, $size, false);
            $image['sizes'][$size] = $s[0];
            $image['sizes'][$size . '-width'] = $s[1];
            $image['sizes'][$size . '-height'] = $s[2];
        }
    }

    return $image;
}
