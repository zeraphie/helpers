<?php

// Add the google api key via .env to acf
add_filter('acf/settings/google_api_key', function () {
    return GOOGLE_API_KEY_FRONTEND;
});
