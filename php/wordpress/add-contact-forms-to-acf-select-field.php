<?php

/**
 * Dynamically populate the field with all the contact forms
 *
 * @param $field
 *
 * @return mixed
 */
function addContactFormsToSelectField($field){
    $field['choices'] = [];

    // Get contact form 7 forms
    $cf7forms = get_posts([
        'post_type' => 'wpcf7_contact_form'
    ]);

    if($cf7forms){
        foreach($cf7forms as $form){
            $selectValue = "[contact-form-7 id='{$form->ID}' title='{$form->post_title}']";

            $field['choices'][$selectValue] = $form->post_title . ' - Contact Form 7';
        }
    }

    if(class_exists('GFFormsModel')){
        // Get gravity forms
        $gforms = \GFFormsModel::get_forms();

        foreach($gforms as $form){
            $selectValue = "[gravityform id='{$form->id}' title='false' description='false']";

            $field['choices'][$selectValue] = $form->title . ' - Gravity Forms';
        }
    }

    return $field;
}
add_filter('acf/load_field/name=contact_form', 'addContactFormsToSelectField');
