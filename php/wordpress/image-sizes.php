<?php

$imageSizes = [
    'banner' => [
        'name' => 'Banner',
        'width' => 2000,
        'height' => 500,
        'crop' => true,
        'description' => '4:1 aspect ratio - for the banner section of the site',
        'selectable' => false
    ],
    'medium-square' => [
        'name' => 'Large Square',
        'width' => 250,
        'height' => 250,
        'crop' => true,
        'description' => '1:1 aspect ratio',
        'selectable' => true
    ],
];

/**
 * Register all the image sizes
 */
foreach($imageSizes as $key => $size){
    add_image_size($key, $size['width'], $size['height'], $size['crop']);

    // If the image is marked as selectable, then add it to the display box in
    // the media gallery
    if($size['selectable']){
        add_filter('image_size_names_choose', function($sizes) use ($key, $size){
            if(!array_key_exists($key, $sizes)){
                $sizes[$key] = $size['name'];
            }

            return $sizes;
        });
    }
}
