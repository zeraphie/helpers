<?php

/**
 * Return or echo the pagination for the loop
 * 
 * Usage:
 *     // With WP_Query
 *     $query = new WP_Query(['posts_per_page' => 5]);
 * 
 *     // Do stuff with the loop
 * 
 *     $pagination = pagination($query->max_num_pages);
 * 
 *     // With get_posts
 *     $pageCount = 5;
 *     $posts = get_posts(['posts_per_page' => $pageCount]);
 *     $totalPages = ceil(count($posts) / $pageCount);
 * 
 *     // Do stuff with the loop
 * 
 *     $pagination = pagination($totalPages);
 * 
 * @param      $totalPages
 * @param bool $echo
 *
 * @return array|string
 */
function pagination($totalPages, $echo = false){
    $big = 999999999; // need an unlikely integer

    $pagination = paginate_links([
        'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $totalPages
    ]);
    
    if($echo){
        echo $pagination;
    }

    return $pagination;
}