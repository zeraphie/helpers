<?php

// Used in render-view.php
define('VIEWS_DIR', TEMPLATEPATH . '/views');

// Exclude files that start with "SAMPLE" or "UNUSED" (uppercase)
foreach(glob(TEMPLATEPATH . '/src/lib/**/*.php') as $_file){
    if(
    !in_array(
        substr(basename($_file), 0, 6),
        ['SAMPLE', 'UNUSED']
    )
    ){
        require_once $_file;
    }
}
