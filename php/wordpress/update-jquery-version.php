<?php

/**
 * Upgrade the version of jquery in wordpress and use the minified or unminified
 * version
 *
 * @param string $version
 * @param bool   $minified
 */
function upgrade_jquery_version($version = null, $minified = true){
    $ver = ($version) ?: '3.2.1';
    $min = ($minified) ? '.min' : '';

    // Remove current jquery version
    wp_deregister_script('jquery');

    // Register the new version
    wp_register_script(
        'jquery',
        "http://ajax.googleapis.com/ajax/libs/jquery/$ver/jquery$min.js",
        false,
        $version
    );

    // Enqueue the new version
    wp_enqueue_script('jquery');
}
