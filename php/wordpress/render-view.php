<?php

/**
 * Render a view in wordpress, set the VIEWS_DIR constant in functions.php like:
 *
 * define('VIEWS_DIR', TEMPLATEPATH . '/views');
 *
 * @param            $file
 * @param array      $vars
 *
 * @return string
 */
function renderView($file, $vars = []){
    if(!array_key_exists('id', $vars)){
        $vars['id'] = get_queried_object_id();
    }

    extract($vars, EXTR_OVERWRITE);

    ob_start();

    include VIEWS_DIR . '/' . $file;

    return ob_get_clean();
}
