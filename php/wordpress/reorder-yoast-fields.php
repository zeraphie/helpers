<?php

/**
 * Place yoast field group at the bottom of the post
 *
 * @return string
 */
function reorder_yoast_fields(){
    return 'low';
}
add_filter('wpseo_metabox_prio', 'reorder_yoast_fields');
