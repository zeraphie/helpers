<?php

/**
 * Replace text within ** with bold text
 *
 * Example: **Bolded** text
 *
 * Transforms into: <b>Bolded</b> text
 *
 * @param $text
 *
 * @return mixed
 */
function boldify($text){
    return preg_replace(
        '#\*{2}(.*?)\*{2}#',
        '<b>$1</b>',
        $text
    );
}
