<?php

/**
 * Get the string between two strings
 *
 * @param        $str
 * @param        $start
 * @param        $end
 * @param string $prefix
 * @param string $suffix
 *
 * @return string
 */
function strBetween($str, $start, $end, $prefix = '', $suffix = ''){
    preg_match(
        '/'
        . preg_quote($prefix, '/')
        . preg_quote($start, '/')
        . '(.*?)'
        . preg_quote($end, '/')
        . preg_quote($suffix, '/')
        . '/s',
        $str,
        $matches
    );

    return (!empty($matches) && isset($matches[1])) ? $matches[1] : '';
}
