<?php

/**
 * Transform a string in the format of hh:mm to a float
 *
 * @param   string $time The time to transform
 *
 * @return  bool|float
 */
function timeToHours($time){
    $hoursMinutes = preg_split('/[.:]/', $time);
    
    if(count($hoursMinutes) <= 1){
        return false;
    }
    
    $hours = intval($hoursMinutes[0], 10);
    $minutes = $hoursMinutes[1] ? intval($hoursMinutes[1], 10) : 0;

    return floatval(number_format($hours + $minutes / 60, 2, '.', ''));
}
