<?php

/**
 * Convert a slug (lower case + '-' separated) to a nice name
 *
 * @param $slug
 *
 * @return string
 */
function niceifySlug($slug){
    return ucwords(str_replace('-', ' ', $slug));
}
