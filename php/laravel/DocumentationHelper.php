<?php

namespace Helpers;

use File;
use Route;

/**
 * This is a helper to interact with the documentation for the project which
 * uses markdown files
 *
 * Documentation markdown (.md) files have a meta at the top of each file which looks
 * like this:
 *
 * <!--: # $icon: description -->
 * <!--: # $order: 40 -->
 *
 * Where the icon is the menu icon used (using google material icon font)
 * and the order is where it is placed in the menu (recommended to use units of
 * 10 to start off with)
 *
 * The title of the page is taken from the file name and transformed accordingly
 * so the files need to be named correctly
 */
class DocumentationHelper {
    // This is where the documentation markdown files should be stored
    public static $path = '/resources/documentation';

    /**
     * Get all the documentation
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getDocs(){
        $docs = collect();

        $files = File::allFiles(base_path() . self::$path);

        foreach($files as $file){
            $doc = File::get($file->getPathName());

            $filename = explode('.' . $file->getExtension(), $file->getFilename())[0];

            // Get the icon for the doc
            preg_match('/\<\!\-\-: # \$icon: (.*)(\-\-\>)/i', $doc, $icon);

            // Get the menu order for the doc
            preg_match('/\<\!\-\-: # \$order: (.*)(\-\-\>)/i', $doc, $order);

            $docs->push(collect([
                'request' => ($filename === 'home') ? 'documentation' : 'documentation/' . $filename,
                'route_name' => ($filename === 'home') ? 'index' : $filename,
                'filename' => $filename,
                'name' => ($filename === 'home') ? 'General' : niceifySlug($filename),
                'slug' => ($filename === 'home') ? '' : '/' . $filename,
                'path' => $file->getPathName(),
                'contents' => $doc,
                'parsed' => parsedown($doc),
                'icon' => (!empty($icon)) ? trim($icon[1]) : 'help',
                'order' => (!empty($order)) ? (int) trim($order[1]) : PHP_INT_MAX,
            ]));
        }

        return $docs->sortBy('order');
    }

    /**
     * Generate the routes for the documentation based on what's in the directory
     */
    public static function generateRoutes(){
        $docs = self::getDocs();

        $docs->each(function($doc){
            Route::get("/documentation{$doc['slug']}", ['as' => "documentation.{$doc['name']}", 'uses' => function() use ($doc){
                $title = $doc['name'];
                $markdown = $doc['parsed'];
                return view('documentation', compact('title', 'markdown'));
            }]);
        });
    }

    /**
     * Generate the menu items for the documentation
     *
     * @return array
     */
    public static function generateMenuItems(){
        $docs = self::getDocs();

        $items = collect();

        $docs->each(function($doc) use ($items){
            $items->push([
                'requestIs' => request()->is($doc['request']),
                'url' => '/' . $doc['request'],
                'text' => $doc['name'],
                'icon' => "<i class='large material-icons'>{$doc['icon']}</i>"
            ]);
        });

        return $items->toArray();
    }
}
