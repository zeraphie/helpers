<?php

/**
 * Render Markdown using Parsedown
 *
 * @param $text
 *
 * @return mixed
 */
function parsedown($text){
    $parser = new Parsedown();

    return $parser->text($text);
}
