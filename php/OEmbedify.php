<?php

namespace Helpers;

use \Embed\Embed;

/**
 * Class OEmbedify
 *
 * A small helper class to automatically add in oembed-able embeds into content
 * requires the package https://github.com/oscarotero/Embed to do the heavy
 * lifting on the embedding
 *
 * @package Helpers
 */
class OEmbedify {
    /**
     * Whitelist regex from WordPress v4.7.2 WP_oEmbed
     *
     * File is at www/wp-includes/class-oembed.php
     *
     * Copy and paste across the new regex only!
     */
    const WHITELIST = [
        '#https?://((m|www)\.)?youtube\.com/watch.*#i',
        '#https?://((m|www)\.)?youtube\.com/playlist.*#i',
        '#https?://youtu\.be/.*#i',
        '#https?://(.+\.)?vimeo\.com/.*#i',
        '#https?://(www\.)?dailymotion\.com/.*#i',
        '#https?://dai\.ly/.*#i',
        '#https?://(www\.)?flickr\.com/.*#i',
        '#https?://flic\.kr/.*#i',
        '#https?://(.+\.)?smugmug\.com/.*#i',
        '#https?://(www\.)?hulu\.com/watch/.*#i',
        'http://i*.photobucket.com/albums/*',
        'http://gi*.photobucket.com/groups/*',
        '#https?://(www\.)?scribd\.com/doc/.*#i',
        '#https?://wordpress\.tv/.*#i',
        '#https?://(.+\.)?polldaddy\.com/.*#i',
        '#https?://poll\.fm/.*#i',
        '#https?://(www\.)?funnyordie\.com/videos/.*#i',
        '#https?://(www\.)?twitter\.com/\w{1,15}/status(es)?/.*#i',
        '#https?://(www\.)?twitter\.com/\w{1,15}$#i',
        '#https?://(www\.)?twitter\.com/\w{1,15}/likes$#i',
        '#https?://(www\.)?twitter\.com/\w{1,15}/lists/.*#i',
        '#https?://(www\.)?twitter\.com/\w{1,15}/timelines/.*#i',
        '#https?://(www\.)?twitter\.com/i/moments/.*#i',
        '#https?://vine\.co/v/.*#i',
        '#https?://(www\.)?soundcloud\.com/.*#i',
        '#https?://(.+?\.)?slideshare\.net/.*#i',
        '#https?://(www\.)?instagr(\.am|am\.com)/p/.*#i',
        '#https?://(open|play)\.spotify\.com/.*#i',
        '#https?://(.+\.)?imgur\.com/.*#i',
        '#https?://(www\.)?meetu(\.ps|p\.com)/.*#i',
        '#https?://(www\.)?issuu\.com/.+/docs/.+#i',
        '#https?://(www\.)?collegehumor\.com/video/.*#i',
        '#https?://(www\.)?mixcloud\.com/.*#i',
        '#https?://(www\.|embed\.)?ted\.com/talks/.*#i',
        '#https?://(www\.)?(animoto|video214)\.com/play/.*#i',
        '#https?://(.+)\.tumblr\.com/post/.*#i',
        '#https?://(www\.)?kickstarter\.com/projects/.*#i',
        '#https?://kck\.st/.*#i',
        '#https?://cloudup\.com/.*#i',
        '#https?://(www\.)?reverbnation\.com/.*#i',
        '#https?://videopress\.com/v/.*#',
        '#https?://(www\.)?reddit\.com/r/[^/]+/comments/.*#i',
        '#https?://(www\.)?speakerdeck\.com/.*#i',
        '#https?://www\.facebook\.com/.*/posts/.*#i',
        '#https?://www\.facebook\.com/.*/activity/.*#i',
        '#https?://www\.facebook\.com/.*/photos/.*#i',
        '#https?://www\.facebook\.com/photo(s/|\.php).*#i',
        '#https?://www\.facebook\.com/permalink\.php.*#i',
        '#https?://www\.facebook\.com/media/.*#i',
        '#https?://www\.facebook\.com/questions/.*#i',
        '#https?://www\.facebook\.com/notes/.*#i',
        '#https?://www\.facebook\.com/.*/videos/.*#i',
        '#https?://www\.facebook\.com/video\.php.*#i',
    ];

    /**
     * Regex for testing if the entire string is a link
     */
    const REGEX_LINK = '|^(\s*)(https?://[^\s<>"]+)(\s*)$|i';

    /**
     * Regex for testing if the entire string is a link wrapped in 'p' tags
     */
    const REGEX_LINK_P = '|(<p(?: [^>]*)?>\s*)(https?://[^\s<>"]+)(\s*<\/p>)|i';

    /**
     * Render the embed version of the link if able to if the link matches the
     * whitelist
     *
     * @param $link
     *
     * @return null|string
     */
    public static function oembed($link){
        try {
            foreach(self::WHITELIST as $pass){
                if(preg_match($pass, $link)){
                    $embed = Embed::create(trim($link))->code;

                    if($embed){
                        $link = "<span class='oembedify-wrapper'>$embed</span>";
                    }

                    break;
                }
            }
        } catch(\Exception $e){
            // The link isn't valid for oembed
        }

        return $link;
    }

    /**
     * If the entire string contains only a link, run it through oembed
     *
     * @param $line
     *
     * @return null|string
     */
    public static function maybeOembed($line){
        if(preg_match(self::REGEX_LINK, $line)){
            return self::oembed(strip_tags($line));
        }

        if(preg_match(self::REGEX_LINK_P, $line)){
            return '<p>' . self::oembed(strip_tags($line)) . '</p>';
        }

        return $line;
    }

    /**
     * Return the filtered content with embeds
     *
     * @param $content
     *
     * @return string
     */
    public static function filter($content){
        $lines = [];

        $rgxLines = preg_split("/((\r?\n)|(\r\n?))/", $content);

        foreach($rgxLines as $line){
            $lines[] = self::maybeOembed($line);
        }

        return implode("\r\n", $lines);
    }
}
