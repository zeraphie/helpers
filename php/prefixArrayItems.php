/**
 * Prefix every item in an array with a given prefix
 *
 * @param array  $array
 * @param string $prefix
 *
 * @return array
 */
function prefixArrayItems(array $array, string $prefix){
    return array_map(function($item) use ($prefix){
        return $prefix . $item;
    }, $array);
}
