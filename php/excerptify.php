<?php

/**
 * Get an excerpt from some content
 *
 * @param     $text
 * @param int $length
 *
 * @return string
 */
function excerptify($text, $length = 140){
    if(strlen($text) > $length){
        $excerpt = substr(
            $text,
            0,
            strpos(
                $text,
                ' ',
                $length
            )
        );

        return trim($excerpt, " \t\n\r\0\x0B.!?,;:") . '&hellip;';
    } else {
        return $text;
    }
}
