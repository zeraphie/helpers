<?php

namespace Helpers;

/**
 * Class Linkify
 *
 * A small helper class to autolink in content
 *
 * @package Helpers
 */
class Linkify {
    /**
     * Regex to get all the links in some content
     */
    const REGEX_GET_LINKS = '#([^"=\'>])((https?)://[^\s<]+[^\s<\.)])#im';

    /**
     * Test if the url is an internal or external one, opening external urls in
     * a new page
     *
     * @param $matches
     *
     * @return string
     */
    public static function replaceLinks($matches){
        if(parse_url($matches[2])['host'] === $_SERVER['HTTP_HOST']){
            return "$matches[1]<a href='$matches[2]'>$matches[2]</a>";
        } else {
            return "$matches[1]<a href='$matches[2]' target='_blank'>$matches[2]</a>";
        }
    }

    /**
     * Replace all links in content with an 'a' tag
     *
     * @param $content
     *
     * @return mixed
     */
    public static function filter($content){
        return preg_replace_callback(
            self::REGEX_GET_LINKS,
            '\Helpers\Linkify::replaceLinks',
            $content
        );
    }
}
