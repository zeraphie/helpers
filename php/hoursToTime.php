<?php

/**
 * Transform a float to a string in hh:mm format
 *
 * This uses the zeroify function in the same directory
 *
 * @param   float|integer $h The time to transform
 *
 * @throws  Exception                   If $h is not a number
 * @return  array|string
 */
function hoursToTime($h){
    if(is_numeric($h)){
        if(strpos((string) $h, '.') !== false){
            $hours = zeroify(intval($h));
            $minutes = zeroify((int) round(60 * floatval($h - $hours)));
            $h = $hours . ':' . $minutes;
        } else {
            $h = ($h < 10) ? '0' . $h . ':00' : $h . ':00';
        }
    } else {
        throw new \Exception($h . ' is not a number');
    }

    return $h;
}
