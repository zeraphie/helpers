<?php

/**
 * Add a leading 0 to an integer if it's below 10
 *
 * @param   integer|float $num The number to add a 0 to
 *
 * @throws  Exception                   If $num is not an integer
 * @return  string
 */
function zeroify($num){
    if(is_int($num)){
        return ($num < 10) ? '0' . $num : $num;
    } else {
        throw new \Exception($num . ' is not an integer');
    }
}