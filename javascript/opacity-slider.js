/*==============================================================================
 Opacity Slider - A really simple opacity slider's javascript
==============================================================================*/
let opacitySliders = $('.slider.opacity-transition');
if(!!opacitySliders.length){
    opacitySliders.each(function(){
        let slider = $(this);
        let slides = slider.find('.slide');

        if(!!slides.length){
            setInterval(function(){
                let currentSlide = slider.find('.slide.active');

                currentSlide.removeClass('active');

                if(currentSlide.index() < slides.length - 1){
                    currentSlide.next().addClass('active');
                } else {
                    slides.first().addClass('active');
                }
            }, 5000);
        }
    });
}