var browser =  {
    info: function(){
        var ua= navigator.userAgent, tem,
        M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if(/trident/i.test(M[1])){
            tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
            return 'IE '+(tem[1] || '');
        }
        if(M[1]=== 'Chrome'){
            tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
            if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
        }
        M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
        if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
        return M.join(' ');
    },
    name: function(){
        var browser = this.info();
        return browser.substr(0,browser.indexOf(' '));
    },
    version: function(){
        var browser = this.info();
        return browser.substr(browser.indexOf(' ')+1)
    },
    matchInfo: function(m){
        var browser = this.info();
        if(browser.match(m)){
            return true;
        } else {
            return false;
        }
    },
    matchName: function(m){
        var browser = this.name();
        if(browser.match(m)){
            return true;
        } else {
            return false;
        }
    },
    matchVersion: function(m){
        var browser = this.version();
        if(browser.match(m)){
            return true;
        } else {
            return false;
        }
    }
};