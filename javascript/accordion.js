/*==============================================================================
 Accordions!! - Javascript solution to responsive accordions
==============================================================================*/
let accordions = $('.accordion');
if(!!accordions.length){
    accordions.each(function(){
        let accordion = $(this);
        let opener = accordion.find('.accordion-opener');
        let content = accordion.find('.accordion-content');
        let timer;

        // Toggle the opening :D
        accordion.find('.accordion-opener').on('click', function(){
            clearTimeout(timer);

            // Have a quick calculation of the height of the elements... Even if
            // the wrapper is max-height!
            let height = Array.prototype.reduce.call(
                content[0].childNodes,
                (position, element) => {
                    // The 20 here is the bottom margin of the elements in this
                    // div.... not sure how to make this better element is all
                    // nodes, not necessarily just tags
                    // TODO: MAKE THIS BETTER.
                    return position + (element.offsetHeight || 0) + 20;
                },
                0
            ) + 'px';

            // Always set the max height to animate properly!
            content.css({
                'max-height': height
            });

            if(!accordion.hasClass('active')){
                // Default it to the initial height so it can remain responsive
                timer = setTimeout(function(){
                    content.css({
                        'max-height': 'initial'
                    });
                }, 310);
            } else {
                // Animate to nothing!
                timer = setTimeout(function(){
                    content.css({
                        'max-height': 0
                    });
                }, 10);
            }

            // Toggle the class so that the padding and opacity is animated via
            // css
            accordion.toggleClass('active');
        });
    });
}