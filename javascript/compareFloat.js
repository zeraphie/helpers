/**
 * Compare two floats using a comparison symbol
 * 
 * Usage:
 *     compareFloat(0.1, '<', 0.2); // true
 *     compareFloat(0.2, '<', 0.1); // false
 *     compareFloat(0.1, '=', 0.1); // true
 *
 * @param float1 {float}
 * @param type {string}
 * @param float2 {float}
 *
 * @returns {boolean}
 */
const compareFloat = (float1, type = '=', float2) => {
    if(Number(float1) !== float1){
        throw new Error('Float 1 "' + float1 + '" is not a number, is of type: ' + typeof float1);
    }
    
    if(Number(float2) !== float2){
        throw new Error('Float 2 "' + float2 + '" is not a number, is of type: ' + typeof float2);
    }
    
    let diff = float1 - float2;
    let abs = Math.abs(diff); // Get the absolute difference
    const e = Number.EPSILON; // Language specific smallest precision for floats
    const validTypes = ['>', '<', '=']; // Whitelist the types to use
    
    if(!validTypes.includes(type)){
        // Throw an error if it's an invalid type
        throw new Error('Invalid type, use one of the following comparisons: ' + validTypes.join(', '));
    }

    if(type === '<'){
        // If the difference is less than e, but the absolute value higher, float1 is less than float 2
        return diff < 0 && abs > e;
    }

    if(type === '>'){
        // If the difference is larger than e, but the absolute value higher than e, float1 is more than float 2
        return diff > 0 && abs > e;
    }

    if(type === '='){
        // If the absolute difference is smaller than e, the floats are "roughly" the same
        return abs < e;
    }
};