/**
 * Shuffle an array using the Fischer-Yates shuffle
 *
 * This modifies the original array
 */
Array.prototype.shuffle = function(){
    var m = this.length, t, i;
    
    while (m) {
        i = Math.floor(Math.random() * m--);
        t = this[m];
        this[m] = this[i];
        this[i] = t;
    }
    
    return this;
}