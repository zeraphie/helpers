import buildQuery from './buildQuery.js';

/**
 * Render an image of the map instead of a map instance, this is better load
 * for maps that solely showcase a location (i.e. no markers) and append it to
 * the map element
 *
 * Note: This requires an element with the following attributes to be set
 *
 * data-lat (the latitude)
 * data-lng (the longitude)
 * data-zoom (the zoom)
 *
 * @param element
 */
export default function renderStaticMap(element){
    let lat = element.data('lat');
    let lng = element.data('lng');
    let zoom = element.data('zoom');

    let params = buildQuery({
        size: '500x400',
        center: lat + ',' + lng,
        zoom: zoom,
        key: GOOGLE_API_KEY
    });

    let url = 'https://maps.googleapis.com/maps/api/staticmap?' + params;

    let image = `<img src="${url}" alt="Static Map" height="400" width="500">`;

    return element.append(image);
}
