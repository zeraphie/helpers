/**
 * Map class that handles creation of a google map instance and its interactions
 */
export default class GoogleMap {
    /**
     * Constructor for the Map class as it must always be instantiated
     *
     * @param mapElement
     */
    constructor(mapElement){
        this.map = null;
        this.mapElement = mapElement;
        this.markers = [];
        this.infoWindow = null;
    }

    /**
     * The default center of the map
     *
     * @returns {{lat: number, lng: number}}
     * @constructor
     */
    static get DEFAULT_CENTER(){
        // This is the latitude and longitude of Oxford, United Kingdom
        return {
            lat: 51.752022,
            lng: -1.257726
        };
    }

    /**
     * The maximum zoom level that the map can reach (i.e. how far in the map
     * will zoom)
     *
     * @returns {number}
     * @constructor
     */
    static get MAXIMUM_ZOOM_LEVEL(){
        return 18;
    }

    /**
     * Initialise the map AFTER the google api has been loaded
     *
     * Load a map by utilising the loadGoogleMapsApi function in ../lib/loadGoogleMapsApi.js
     *
     * @returns {GoogleMap}
     */
    renderMap(){
        let self = this;

        self.map = new google.maps.Map(
            self.mapElement,
            {
                center: self.constructor.DEFAULT_CENTER,
                zoom: 14,
                maxZoom: self.constructor.MAXIMUM_ZOOM_LEVEL,
                mapTypeId: 'roadmap',
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                },
            }
        );

        // Setup the info window for the api to use
        this.infoWindow = new google.maps.InfoWindow();

        this.usability();

        $(window).resize(function(){
            self.usability();
        });

        return this;
    }

    /**
     * Center the map on a coordinate without having a marker
     *
     * @param lat
     * @param lng
     * @param zoom
     * @returns {GoogleMap}
     */
    centerMap(lat, lng, zoom){
        let latlng = new google.maps.LatLng(
            parseFloat(lat),
            parseFloat(lng)
        );

        this.map.setCenter(latlng);

        this.map.setZoom(parseInt(zoom));

        return this;
    }

    /**
     * Make the marker on the map from the result
     *
     * @param location
     * @returns {google.maps.LatLng}
     */
    makeMarker(location){
        let self = this;

        // This is the actual html of the marker that is added inside the
        // infoWindow
        let html = `${location.address}<br />`;

        // The latitude and longtitude coordinates in google format
        let latlng = new google.maps.LatLng(
            parseFloat(location.lat),
            parseFloat(location.lng)
        );

        // The actual marker itself in google form
        let marker = new google.maps.Marker({
            map: this.map,
            position: latlng
        });

        // Fire the infoWindow opening on clicking the marker
        google.maps.event.addListener(marker, 'click', function() {
            self.infoWindow.setContent(html);
            self.infoWindow.open(self.map, marker);
        });

        // Add the marker to the markers array
        self.markers.push(marker);

        return latlng;
    }

    /**
     * Set the markers on the map
     *
     * @param markers
     * @param callback
     * @returns {GoogleMap}
     */
    setMarkers(markers, callback){
        let self = this;

        let bounds = new google.maps.LatLngBounds();

        // Make sure to have no markers set before setting them!
        self.resetMarkers();

        // If there are markers, place them, then after all the markers are
        // placed, center the map on the markers so all of them are visible
        if(!!markers.length){
            markers.forEach(function(location){
                bounds.extend(self.makeMarker(location));
            });

            self.map.fitBounds(bounds);
        }

        self.usability();

        if(typeof callback === 'function'){
            callback(markers);
        }

        return self;
    }

    /**
     * Empty the markers in the map
     */
    /**
     * Empty the markers in the map
     *
     * @returns {GoogleMap}
     */
    resetMarkers(){
        for (let i = 0, len = this.markers.length; i < len; i++) {
            this.markers[i].setMap(null);
        }

        this.markers.length = 0;

        this.usability();

        return this;
    }

    /**
     * Remove drag and scroll functionality if screen is below 768px or if there
     * are no markers
     *
     * @returns {GoogleMap}
     */
    usability(){
        // Remove draggable functionality if screen is below 768px or if there
        // are no markers
        let desktop = ($(document).width() > 768) && (!!this.markers.length);

        this.map.setOptions({
            draggable: desktop,
            scrollwheel: desktop
        });

        return this;
    }
}
