/**
 * Calculate the addorial of a number, i.e.
 *
 * addorial(4) = 4 + 3 + 2 + 1
 *
 * @param integer number The number to start from
 *
 * @returns integer|boolean
 */
const addorial = (number) => {
    // Has to be a number
    if(typeof number !== 'number'){
        return false;
    }

    // Has to be an integer
    if(!Number.isInteger(number)){
        return false;
    }

    // Has to be more than or equal to 1
    if(number < 1){
        return false;
    }

    let total = 0;
    
    while(number--){
        total += number + 1;
    }

    return total;
}