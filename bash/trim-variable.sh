# Trim a variable of line endings, useful if dos2unix
# hasn't been run or can't be run

a=${a%$'\r'}