# Change permissions of files so they're writable
# by the user and group but not others
chmod ug+rwX,o+rX-w -R . &&

# Exclude extra directories
chmod o-rwx -R www/wp-content/upgrade &&

# PHP files permissions
find . -name '*.php' -exec chmod o-rwx '{}' +