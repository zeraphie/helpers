# Test if a variable exists, then do something with it

if [ -n "${variable+1}" ]; then
    # Do something if it exists
fi