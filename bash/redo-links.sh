# MySQL script to run after downloading the live database to development server
# In order to update the links and email addresses
#
# This script is stored in the mysql_script bash variable to be used later on

read -r -d '' mysql_script <<END_MYSQL_SCRIPT
    UPDATE ${DS_DBPREFIX}options
    SET option_value = '$GENERAL_EMAIL'
    WHERE option_name = 'admin_email';
    
    UPDATE ${DS_DBPREFIX}users
    SET user_email = CONCAT("$USER+user-", ID, "@$GENERAL_EMAIL_DOMAIN");
    
    UPDATE ${DS_DBPREFIX}posts
    SET post_content = REPLACE(post_content, '$DS_FROM_URL', '$DS_TO_URL')
END_MYSQL_SCRIPT