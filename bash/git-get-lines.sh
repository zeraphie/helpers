# Get number of lines in a git project via http://stackoverflow.com/a/28858550

git diff --shortstat `git hash-object -t tree /dev/null`