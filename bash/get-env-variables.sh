# Use .env variables in a script, they may need to be
# trimmed if the .env is in windows using the trim.sh
# function, or use dos2unix on the .env file

source <(sed -E -n 's/^[^#]+/export &/ p' .env)